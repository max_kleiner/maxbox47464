****************************************************************
Release Notes maXbox 4.7.4.64 June 2020 mX47
****************************************************************

1254 unit uPSI_MaskEdit.pas    FCL
1255 unit uPSI_SimpleRSSTypes; BlueHippo
1256 unit uPSI_SimpleRSS;      BlueHippo
1257 unit uPSI_psULib.pas      Prometheus
1258 unit uPSI_psUFinancial;   Prometheus
1259 uPSI_PsAPI_2.pas          mX4
1260 uPSI_PersistSettings_2    mX4
1261 uPSI_rfc1213util2.pas     IP
1262 uPSI_JTools.pas           JCL
1263 unit uPSI_neuralbit.pas   CAI
1264 unit uPSI_neuralab.pas    CAI
1265 unit uPSI_winsvc2.pas     TEK
1266 unit uPSI_wmiserv2.pas    TEK
1267 uPSI_neuralcache.pas      CAI
1268 uPSI_neuralbyteprediction CAI
1269 unit uPSI_USolarSystem;   glscene.org
1270 uPSI_USearchAnagrams.pas  DFF
1271 uPSI_JsonsUtilsEx.pas     Randolph
1272 unit uPSI_Jsons.pas       Randolph
1273 unit uPSI_HashUnit;       DFF
1274 uPSI_U_Invertedtext.pas   DFF
1275 unit uPSI_Bricks;         Dendron
1276 unit uPSI_lifeblocks.pas  Dendron

Totals of Function Calls: 32633
SHA1: of 4.7.4.64 DA4C716E31E2A4298013DFFBDA7A98D48650B0C7
CRC32: 3EB27A87:  28.2 MB (29,608,248) bytes

**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).